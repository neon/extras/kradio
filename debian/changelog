kradio4 (4.0.8+git20180527-1) unstable; urgency=medium

  * New upstream Git snapshot:
    - fixes build with FFmpeg 4.0+ (Closes: #888352)
  * Switch Vcs-* fields to salsa.debian.org.
  * Remove trailing whitespaces in changelog.
  * Bump the debhelper compatibility to 11:
    - bump the debhelper build dependency to 11~
    - bump compat to 11
    - remove --parallel for dh, as now done by default
  * Bump Standards-Version to 4.1.4, no changes required.
  * Switch from the deprecated libavresample to libswresample: replace
    the libavresample-dev build dependency with libswresample-dev.

 -- Pino Toscano <pino@debian.org>  Sun, 27 May 2018 17:46:43 +0200

kradio4 (4.0.8+git20170124-1) unstable; urgency=medium

  * New upstream Git snapshot.

 -- Pino Toscano <pino@debian.org>  Tue, 24 Jan 2017 22:58:43 +0100

kradio4 (4.0.8+git20170114-1) unstable; urgency=medium

  * New upstream Git snapshot.
  * The rebuild with newer lirc-client should fix crashes in the lirc plugin.
    (Closes: #844517)

 -- Pino Toscano <pino@debian.org>  Sat, 14 Jan 2017 23:33:56 +0100

kradio4 (4.0.8+git20160618-1) unstable; urgency=medium

  * New upstream Git snapshot.
  * Bump Standards-Version to 3.9.8, no changes required.
  * Update copyright.

 -- Pino Toscano <pino@debian.org>  Sat, 18 Jun 2016 23:40:25 +0200

kradio4 (4.0.8+git20160325-1) unstable; urgency=medium

  * New upstream Git snapshot.
  * Remove /usr/sbin handling from rules, since the upstream debian directory
    now is in a subdirectory.
  * Bump Standards-Version to 3.9.7, no changes required.
  * Use https for the Vcs-Browser field.

 -- Pino Toscano <pino@debian.org>  Fri, 25 Mar 2016 09:46:16 +0100

kradio4 (4.0.8-2) unstable; urgency=medium

  * Drop menu file and its pixmap, since kradio4 already provides a .desktop
    file.
  * Bump Standards-Version to 3.9.6, no changes required.
  * Drop the transitional kradio package, available as such in Wheezy and
    Jessie.
  * Make kradio4 provide kradio.

 -- Pino Toscano <pino@debian.org>  Mon, 11 Jan 2016 17:44:10 +0100

kradio4 (4.0.8-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdelibs5-dev build dependency to >= 4.7.0, as indicated
    in the upstream build system.
  * Ignore failure when removing /usr/sbin (might not exist).

 -- Pino Toscano <pino@debian.org>  Sun, 28 Sep 2014 13:06:53 +0200

kradio4 (4.0.7+git20140816-1) unstable; urgency=medium

  * New upstream Git snapshot:
    - fix sound playback with Internet radios. (Closes: #750779)
  * Bump Standards-Version to 3.9.5, no changes required.
  * Enable the parallel build.
  * Add myself to Uploaders.
  * Add the libavresample-dev build dependency.
  * Fix removal of empty /usr/sbin.

 -- Pino Toscano <pino@debian.org>  Sat, 16 Aug 2014 12:49:07 +0200

kradio4 (4.0.7-2) unstable; urgency=low

  * Upload to unstable
  * Update debian/control - fix vcs-field-not-canonical
  * Update Standards-Version: 3.9.4 - no changes

 -- Mark Purcell <msp@debian.org>  Sun, 12 May 2013 09:34:04 +1000

kradio4 (4.0.7-1) experimental; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 02 Feb 2013 17:11:22 +1100

kradio4 (4.0.6-2) experimental; urgency=low

  * Added Build-Depends: libboost-dev - fixes FTBFS

 -- Mark Purcell <msp@debian.org>  Mon, 05 Nov 2012 10:57:15 +1100

kradio4 (4.0.6-1) experimental; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 08 Sep 2012 13:45:35 +1000

kradio4 (4.0.4-1) unstable; urgency=low

  * New upstream release
  * New maintainer KDE Extras Team (Closes: #655255)
  * Add Vcs fields
  * Drop obsolete upstream_fixed-segfault-with-recent-libav-libffmpeg.patch

 -- Mark Purcell <msp@debian.org>  Sun, 19 Feb 2012 14:26:32 +1100

kradio4 (4.0.2-6) unstable; urgency=low

  * QA upload.
  * Replace patch kradio-fix-liva07-compat.patch with the backport of an
    upstream commit, which should fix a crash with libav 0.7.
    (Closes: #641809)

 -- Pino Toscano <pino@debian.org>  Sat, 01 Oct 2011 14:44:21 +0200

kradio4 (4.0.2-5) unstable; urgency=low

  * QA upload.
  * Add patch kradio-fix-liva07-compat.patch to remove the usage of
    deprecated ffmpeg/libav API. This prevents a FTFBS when
    Debian switches to libav 0.7.1, which finally removes these
    API elements (Closes: #638563)

 -- Moritz Muehlenhoff <jmm@debian.org>  Sun, 21 Aug 2011 14:50:28 +0200

kradio4 (4.0.2-4) unstable; urgency=low

  [ Pino Toscano ]
  * QA upload.
  * Build-depend on libmp3lame-dev to get MP3 recording support, and
    update the description accordingly.

 -- Pino Toscano <pino@debian.org>  Sun, 14 Aug 2011 11:06:40 +0200

kradio4 (4.0.2-3) unstable; urgency=low

  [ Pino Toscano ]
  * QA upload.
  * Use wildcard architectures:
    - !kfreebsd-i386 !kfreebsd-amd64 !hurd-i386 -> linux-any
  * Bump Standards-Version to 3.9.2, no changes required.
  * Remove changelog installed by upstream, dh_installchangelogs will do it.
  * Switch to my @debian.org address, I'm a DD now.

 -- Pino Toscano <pino@debian.org>  Sun, 17 Jul 2011 13:30:42 +0200

kradio4 (4.0.2-2) unstable; urgency=low

  [ Pino Toscano ]
  * QA upload.
  * Upload to unstable. (Closes: #604331)

 -- Pino Toscano <pino@kde.org>  Sun, 06 Feb 2011 22:24:09 +0100

kradio4 (4.0.2-1) experimental; urgency=low

  [ Pino Toscano ]
  * QA upload.
  * New upstream release. (Closes: #609258)
  * Bring back and update the kradio4-convert-presents.1 man page.
  * Update copyright.
  * debian/rules: pass first the sequence to `dh', then the arguments.

 -- Pino Toscano <pino@kde.org>  Sun, 16 Jan 2011 10:37:20 +0100

kradio4 (4.0.1-1) experimental; urgency=low

  [ Pino Toscano ]
  * QA upload.
  * New upstream release:
    - Fixes build on armel and with newer avutils. (Closes: #597700)
  * Add libglib2.0-dev to the build-dependencies, otherwise querying libmms
    via pkg-config cannot be done.
  * Update copyright.
  * Small touches to the descriptions.
  * Small touches to the menu file.
  * Update a bit the man page.
  * Bump Standards-Version to 3.9.1, no changes required.

 -- Pino Toscano <pino@kde.org>  Sat, 16 Oct 2010 15:08:56 +0200

kradio4 (4.0.0-1) experimental; urgency=low

  [ Pino Toscano ]
  * QA upload.
  * New upstream release, for KDE 4:
    - Should fix crash on startup with ALSA disabled and OSS used instead.
      (Closes: #461313)
  * Rename source and main binary package to kradio4, following upstream
    renaming:
    - Add dummy transitional package kradio.
    - Follow renaming in man pages, menu file.
  * Update build-dependencies:
    - Add pkg-kde-tools >= 0.5, cmake, kdelibs5-dev >= 4.2.0, gettext,
      libasound2-dev, libmms-dev, libvorbis-dev, libogg-dev, libavformat-dev.
    - Remove imagemagick, automake, libtool, autotools-dev.
  * Convert to dh7 + kde addon:
    - Bump debhelper build-dependency to 7.3.16.
    - Bump compat to 7.
    - Simplify rules a bit.
  * Switch to the "3.0 (quilt)" format, so we can avoid the debian directory
    in the upstream tarball.
  * Update watch file. (Closes: #449777)
  * Bump Standards-Version to 3.8.4, no changes required.
  * Update description.
  * Add ${misc:Depends}.
  * Remove the man page convert-presets.1, no more used.
  * Minor update of the man page kradio4.1.
  * Disable the ALSA stuff on non-Linux architectures.
  * Make sure to not ship the COPYING and INSTALL files that are installed
    upstream.
  * Include a converted version of the kradio4 icon to XPM, so there is no need
    to build-depend on imagemagick and generate it at build time.
  * Update copyright a bit.

 -- Pino Toscano <pino@kde.org>  Thu, 03 Jun 2010 16:41:22 +0200

kradio (0.1.1.1~20061112-4) unstable; urgency=low

  * QA upload ; orphaning after discussion with maintainer
  * fix up autoconf things (Closes: 543013)
  * Relibtoolize while we are at it.
  * Switch to unversioned autotools.
  * Clean up rules file.
  * Move homepage from description to dedicated field
  * Bump S-V
  * Update menu file

 -- Sune Vuorela <debian@pusling.com>  Tue, 01 Jun 2010 13:20:29 +0200

kradio (0.1.1.1~20061112-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix gcc-4.3 FTBFS, patch by Kibi (Closes: #455390)

 -- Marc 'HE' Brockschmidt <he@debian.org>  Sun, 16 Mar 2008 19:00:02 +0100

kradio (0.1.1.1~20061112-3) unstable; urgency=low

  * Modify build-system to use automake
   - Remove build-depends against unsermake as it shall be removed from
     debian soon
   - Add build-depends against automake1.9
   - Recreate the autofoo files
   - Add build-depends against chrpath and use it to remove some rpaths
  * Make sure that all the .gmo generated files are deleted to avoid
    the FTBFS during a second build (Closes: #424463)
  * Dependencies are accurate for the new libflac libraries
    (Closes: #426649)

 -- Steffen Joeris <white@debian.org>  Wed, 20 Jun 2007 22:14:59 +0200

kradio (0.1.1.1~20061112-2) unstable; urgency=low

  * Upload to unstable

 -- Steffen Joeris <white@debian.org>  Mon, 23 Apr 2007 05:59:03 +1000

kradio (0.1.1.1~20061112-1) experimental; urgency=low

  * New upstream release
  * Fix small typo in debian/copyright
  * Bump debhelper level to 5
  * Change my maintainer address

 -- Steffen Joeris <white@debian.org>  Sun,  7 Jan 2007 18:42:05 +0100

kradio (0.1.1.1~20060920-1) unstable; urgency=low

  * New upstream version
  * Drop 10-configure.dpatch
  * Drop dpatch from debian/rules and debian/control
  * Add build-depends against unsermake
  * Adjust debian/rules for cleaning up properly
  * Add temporary dependency, because unsermake is broken

 -- Steffen Joeris <steffen.joeris@skolelinux.de>  Tue,  3 Oct 2006 15:01:07 +1000

kradio (0.1beta1.0snapshot20051127-2) unstable; urgency=low

  * Upload to unstable, no changes

 -- Steffen Joeris <steffen.joeris@skolelinux.de>  Sun,  5 Feb 2006 18:42:11 +0100

kradio (0.1beta1.0snapshot20051127-1) experimental; urgency=low

  * Initial release for Debian (Closes: #341954)
  * drop debian dir from upstream and use own debian dir

 -- Steffen Joeris <steffen.joeris@skolelinux.de>  Sun,  4 Dec 2005 12:01:26 +0000
